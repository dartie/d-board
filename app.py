from flask import Flask, render_template, request, jsonify, send_from_directory
from flask_pretty import Prettify
from werkzeug.routing import BaseConverter
import os
import sys
import time
import inspect
from flask_sqlalchemy import SQLAlchemy
import datetime
import time
import threading
import json
import unidecode
import traceback
import importlib

# import Namespace
try:
    from types import SimpleNamespace as Namespace  # available from Python 3.3
except ImportError:
    class Namespace:
        def __init__(self, **kwargs):
            self.__dict__.update(kwargs)


class RegexConverter(BaseConverter):
    def __init__(self, url_map, *items):
        super(RegexConverter, self).__init__(url_map)
        self.regex = items[0]


app = Flask(__name__, template_folder='jinja-templates')
prettify = Prettify(app)
app.url_map.converters['regex'] = RegexConverter


def get_last_modified_date(data_file):
    # fileStatsObj = os.stat(data_file)
    # modificationTime = time.ctime(fileStatsObj[os.path.stat.ST_MTIME])

    modTimesinceEpoc = os.path.getmtime(data_file)
    modificationTime = datetime.datetime.utcfromtimestamp(modTimesinceEpoc).strftime('%Y-%m-%d %H:%M:%S')
    # print("Last Modified Time : ", modificationTime, ' UTC')

    javascript_format = datetime.datetime.utcfromtimestamp(modTimesinceEpoc).strftime('%m/%d/%Y %I:%M:%S %p ') + "UTC"  # ('%m-%d-%Y %H:%M:%S %p %Z') # python incorrectly returns %Z empty, so it needs to be added manually

    return javascript_format


@app.template_filter('read_data')
def read_data(module):
    data_file = os.path.join("modules", module, "data.html")

    if os.path.exists(data_file):
        with open(data_file, 'r', encoding='utf-8', errors='ignore') as read_data:
            data_html = read_data.read()

        # get last modified date of the file
        modificationTime = get_last_modified_date(data_file)

    else:
        data_html = "{} module missing".format(module)
        modificationTime = ''

    return [data_html, modificationTime]


def retrieve_data(module):
    module_name = os.path.splitext(module)[0]

    data = {}
    exec("from modules.{} import main".format(module_name))
    start_time = time.time()
    exec("data['a'] = main()")
    print("--- {module}: {exe_time} seconds ---".format(module=module, exe_time=round(time.time() - start_time, 2)))
    
    return data['a']


@app.route('/', methods=['POST', 'GET'])
def index():
    settings_json = 'settings.json'
    templates_dir = 'jinja-templates'

    # read settings
    settings_list = json.loads(open(settings_json, 'r').read())
    return render_template('dynamic-content.html.jinja2', settings_list=settings_list, sidebar=True)


@app.route(r'/<regex(".*\.(png|ico)"):file>')
def favicon(file):
    file = os.path.basename(file)
    return send_from_directory(os.path.join(app.root_path, 'static', 'icons'), file, mimetype='image/vnd.microsoft.icon')


def retrieve_module_data(wg):
    wg_module = wg['module']

    return retrieve_data(wg_module)


def update_modules_info(settings_json='settings.json'):
    while True:
        settings_list = json.loads(open(settings_json, 'r').read())

        threads = []

        for row in settings_list:
            row_widgets_dict = row['widgets']

            for wg in row_widgets_dict:
                # retrieve_module_data(wg)
                t = threading.Thread(target=retrieve_module_data, args=(wg,))
                t.start()

                threads.append(t)

        for th in threads:
            th.join()

        time.sleep(40)


@app.route('/update', methods=['POST', 'GET'])
def refresh_display_info(settings_json='settings.json'):
    settings_list = json.loads(open(settings_json, 'r').read())

    data_updated = {}

    for row in settings_list:
        row_n = row['row']
        row_size = row['size']
        row_widgets_dict = row['widgets']

        for wg in row_widgets_dict:
            module = wg['module']
            title = wg['title']

            data_updated[module] = read_data(module) + [title]

    return jsonify(data_updated)


if __name__ == '__main__':
    settings_json = 'settings.json'

    update_info_modules_thread = threading.Thread(target=update_modules_info, args=(settings_json,))
    update_info_modules_thread.start()

    app.run(host="0.0.0.0", debug=True)

# TODO: fix double request
