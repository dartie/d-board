import os
import sys
import jinja2
import json


def retrieve_data(module):
    data = {}
    exec("from modules.{} import main".format(os.path.splitext(module)[0]))
    exec("data['a'] = main()")
    
    return data['a']


def main(args):
    settings_json = 'settings.json'
    templates_dir = 'jinja-templates'

    # read settings
    settings_list = json.loads(open(settings_json, 'r').read())
    
    # create html file
    template_env = jinja2.Environment(loader=jinja2.FileSystemLoader(
        templates_dir, encoding='utf-8', followlinks=False),
                                      trim_blocks=True,
                                      lstrip_blocks=True,
                                      autoescape=False)
    template_env.filters['retrieve_data'] = retrieve_data  # add custom filter
    loaded_template = template_env.get_template(
        "dynamic-content.html.jinja2")  # template file to render
    result = loaded_template.render(
        settings_list=settings_list
    )  # list of variable=value to pass to the template file to call

    with open('page.html', 'w', encoding="utf-8", errors='ignore') as write_page:
        write_page.write(result)


if __name__ == "__main__":
    main(args=None)