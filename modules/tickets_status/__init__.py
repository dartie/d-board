import os
import sys
import pandas as pd

sys.path.insert(1, os.path.dirname(os.path.dirname(__file__)))
#print(sys.path)

from sf_api import sf_connection, describe_table, query, result_to_dict


def retrieve_data(sf, query_string, refresh=True):
    cache_filename = os.path.join('sf_cache', 'qac-cases.csv')

    if not os.path.exists(cache_filename):
        os.makedirs(os.path.dirname(cache_filename))

    if not refresh and os.path.exists(cache_filename):
        records_dataframe = pd.read_csv(cache_filename)
    else:
        records = query(sf, None, None, None, query_string)
        cases_dict = result_to_dict(records, 'Id')
        records_dataframe = pd.DataFrame(records, index=None)
        records_dataframe.to_csv(cache_filename)

    return records_dataframe


def get_reference_dict(sf, table):
    # get User table
    query_string = "SELECT Id, Name FROM " + table
    result = query(sf, None, None, None, query_string)
    result_dict = result_to_dict(result, 'Id')
    get_reference = {}
    get_referenceid = {}
    for id, info in result_dict.items():
        get_reference[id] = info['Name']
        get_referenceid[info['Name']] = id

    return get_reference, get_referenceid


def get_case_htmllink(row):
    html_case_id = ' <a href="https://perforce.my.salesforce.com/{CaseId}"  target="_blank"> {CaseNumber} </a> '.format(
        CaseId=row['Id'],
        CaseNumber=row['CaseNumber']
    )

    return html_case_id


def main():
    sf = sf_connection()

    progressbar_colors = {
        "On Cust": "bg-success",
        "On Perforce": "bg-danger",
        "Pending Fix": "bg-info",
        "New": "bg-warning"
    }

    # OwnerId != '005A0000001I4eUIAS' excludes Perforce APIUser
    query_string = """SELECT Id, Business_Line__c, Department__c, OwnerId, Status, CaseNumber
FROM Case WHERE 
Business_Line__c = 'QAC' AND 
Department__c = 'Technical Support' AND
(Status = 'On Cust' OR Status = 'On Perforce' OR Status = 'Pending Fix' OR Status = 'New') AND OwnerId != '005A0000001I4eUIAS'
"""
    cases_dataframe = retrieve_data(sf, query_string, refresh=True)
    groupby_field = 'Status'
    cases_grouped = cases_dataframe.groupby([groupby_field]).groups
    groups_counts_tmp = cases_dataframe.groupby([groupby_field]).size().reset_index(name='counts').to_dict()
    groups_counts = {}
    keys = groups_counts_tmp[groupby_field]
    values = groups_counts_tmp["counts"]

    for i, k in keys.items():
        groups_counts[k] = values[i]

    open_cases_html = ["""<div class="progress">"""]
    open_cases_table_html = ["""<table class="table">
	<thead>
		<tr>
			<th scope="col">Status</th>
			<th scope="col">Count</th>
			<th scope="col">Perc %</th>
		</tr>
	</thead>
	<tbody>
"""]

    # calculate total number of cases
    total = sum(groups_counts.values())

    # calculate % for each group
    for g, count in groups_counts.items():
        perc = (100 * count) / total
        perc_round = round(perc, 2)
        groups_counts[g] = count, perc
        color = progressbar_colors.get(g, "")

        open_cases_html_string = """{ind}<div class="progress-bar {color}" role="progressbar" style="width: {perc}%" aria-valuenow="{perc}" aria-valuemin="0" aria-valuemax="100">{perc}% - {g}</div>""".format(ind=" " * 24, perc=perc_round, g=g, color=color)
        open_cases_html.append(open_cases_html_string)

        open_cases_table_html_string = """		<tr>
			<td class="{color}">{status}</td>
			<td class="{color}">{count}</td>
			<td class="{color}">{perc} %</td>
		</tr>
""".format(status=g, count=count, perc=perc_round, color=color)

        open_cases_table_html.append(open_cases_table_html_string)

    # close tags for the above elements
    open_cases_html.append("</div>")
    open_cases_table_html.append("""	</tbody>
</table>
""")

    full_html = open_cases_html 
    full_html_str = "\n".join(full_html)

    # write data to file
    output_file = os.path.join(os.path.dirname(__file__), "data.html")
    try:
        os.remove(output_file)
    except:
        pass
    with open(output_file, 'w', encoding='utf-8', errors="ignore") as write_data:
        write_data.write(full_html_str)

    return full_html_str

#main()